angular.module('firebaseConfig', ['firebase'])

.run(function(){

  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyAX_ifen5hPjX8_YRIWTPoYPkI1kRD9DKg",
    authDomain: "greenfund-1491271189468.firebaseapp.com",
    databaseURL: "https://greenfund-1491271189468.firebaseio.com",
    storageBucket: "greenfund-1491271189468.appspot.com",
  };
  firebase.initializeApp(config);

})

/*

.service("TodoExample", ["$firebaseArray", function($firebaseArray){
    var ref = firebase.database().ref().child("todos");
    var items = $firebaseArray(ref);
    var todos = {
        items: items,
        addItem: function(title){
            items.$add({
                title: title,
                finished: false
            })
        },
        setFinished: function(item, newV){
            item.finished = newV;
            items.$save(item);
        }
    }
    return todos;
}])

*/