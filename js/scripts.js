angular.module('greenfund', ['ionic'])
.controller('PopupCtrl',function($scope, $ionicPopup, $timeout) {

 // Triggered on a button click, or some other target
 $scope.showPopup = function() {
   $scope.data = {}

   // An elaborate, custom popup
   var myPopup = $ionicPopup.show({
     template: '<input type="text" ng-model="data.reward">',
     title: 'Confirmar puntos a canjear',
     subTitle: 'Introduce el número de puntos que deseas canjear para obtener esta recompensa.',
     scope: $scope,
     buttons: [
       { text: 'Cancelar' },
       {
         text: '<b>Canjear</b>',
         type: 'button-positive',
         onTap: function(e) {
           if (!$scope.data.wifi) {
             //don't allow the user to close unless he enters reward
             e.preventDefault();
           } else {
             return $scope.data.wifi;
           }
         }
       },
     ]
   });
   myPopup.then(function(res) {
     console.log('Tapped!', res);
   });
   $timeout(function() {
      myPopup.close(); //close the popup after 3 seconds for some reason
   }, 3000);
  };
   // A confirm dialog
   $scope.showConfirm = function() {
     var confirmPopup = $ionicPopup.confirm({
       title: 'Obtener recompensa',
       template: '¿Estás seguro de que deseas obtener esta recompensa?'
     });
     confirmPopup.then(function(res) {
       if(res) {
         console.log('Sí');
       } else {
         console.log('No');
       }
     });
   };

   // An alert dialog
   $scope.showAlert = function() {
     var alertPopup = $ionicPopup.alert({
       title: '¡Gracias!',
       template: 'Recompensa obtenida'
     });
     alertPopup.then(function(res) {
       console.log('¡Tus puntos han sido canjeados con éxito!');
     });
   };
});