angular.module('app.routes', ['ionicUIRouter'])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
  

      .state('cameraTabDefaultPage', {
    url: '/page2',
    templateUrl: 'templates/cameraTabDefaultPage.html',
    controller: 'cameraTabDefaultPageCtrl'
  })

  .state('eventos', {
    url: '/page18',
    templateUrl: 'templates/eventos.html',
    abstract:true
  })

  .state('registroEmpresa', {
    url: '/page1',
    templateUrl: 'templates/registroEmpresa.html',
    controller: 'registroEmpresaCtrl'
  })

  .state('iniciarSesiNEmpresa', {
    url: '/page10',
    templateUrl: 'templates/iniciarSesiNEmpresa.html',
    controller: 'iniciarSesiNEmpresaCtrl'
  })

  .state('iniciarSesiN', {
    url: '/page9',
    templateUrl: 'templates/iniciarSesiN.html',
    controller: 'iniciarSesiNCtrl'
  })

  .state('registro', {
    url: '/page8',
    templateUrl: 'templates/registro.html',
    controller: 'registroCtrl'
  })

  /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='eventos.unHabitanteUnRbol'
      2) Using $state.go programatically:
        $state.go('eventos.unHabitanteUnRbol');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page18/tab2/page11
      /page18/tab1/page11
      /page18/tab3/page11
  */
  .state('eventos.unHabitanteUnRbol', {
    url: '/page11',
    views: {
      'tab2': {
        templateUrl: 'templates/unHabitanteUnRbol.html',
        controller: 'unHabitanteUnRbolCtrl'
      },
      'tab1': {
        templateUrl: 'templates/unHabitanteUnRbol.html',
        controller: 'unHabitanteUnRbolCtrl'
      },
      'tab3': {
        templateUrl: 'templates/unHabitanteUnRbol.html',
        controller: 'unHabitanteUnRbolCtrl'
      }
    }
  })

  /* 
    The IonicUIRouter.js UI-Router Modification is being used for this route.
    To navigate to this route, do NOT use a URL. Instead use one of the following:
      1) Using the ui-sref HTML attribute:
        ui-sref='eventos.tallerDeEducaciNAmbientalParaNiOs'
      2) Using $state.go programatically:
        $state.go('eventos.tallerDeEducaciNAmbientalParaNiOs');
    This allows your app to figure out which Tab to open this page in on the fly.
    If you're setting a Tabs default page or modifying the .otherwise for your app and
    must use a URL, use one of the following:
      /page18/tab2/page30
      /page18/tab1/page30
      /page18/tab3/page30
  */
  .state('eventos.tallerDeEducaciNAmbientalParaNiOs', {
    url: '/page30',
    views: {
      'tab2': {
        templateUrl: 'templates/tallerDeEducaciNAmbientalParaNiOs.html',
        controller: 'tallerDeEducaciNAmbientalParaNiOsCtrl'
      },
      'tab1': {
        templateUrl: 'templates/tallerDeEducaciNAmbientalParaNiOs.html',
        controller: 'tallerDeEducaciNAmbientalParaNiOsCtrl'
      },
      'tab3': {
        templateUrl: 'templates/tallerDeEducaciNAmbientalParaNiOs.html',
        controller: 'tallerDeEducaciNAmbientalParaNiOsCtrl'
      }
    }
  })

  .state('eventos.estaSemana', {
    url: '/page13',
    views: {
      'tab2': {
        templateUrl: 'templates/estaSemana.html',
        controller: 'estaSemanaCtrl'
      }
    }
  })

  .state('eventos.prXimosEventos', {
    url: '/page31',
    views: {
      'tab1': {
        templateUrl: 'templates/prXimosEventos.html',
        controller: 'prXimosEventosCtrl'
      }
    }
  })

  .state('eventos.eventosPasados', {
    url: '/page32',
    views: {
      'tab3': {
        templateUrl: 'templates/eventosPasados.html',
        controller: 'eventosPasadosCtrl'
      }
    }
  })

  .state('favoritos', {
    url: '/page25',
    templateUrl: 'templates/favoritos.html',
    controller: 'favoritosCtrl'
  })

  .state('crearCuenta', {
    url: '/page16',
    templateUrl: 'templates/crearCuenta.html',
    controller: 'crearCuentaCtrl'
  })

  .state('crearEvento', {
    url: '/page18',
    templateUrl: 'templates/crearEvento.html',
    controller: 'crearEventoCtrl'
  })

  .state('perfil', {
    url: '/page17',
    templateUrl: 'templates/perfil.html',
    controller: 'perfilCtrl'
  })

  .state('altaDeEmpleados', {
    url: '/page21',
    templateUrl: 'templates/altaDeEmpleados.html',
    controller: 'altaDeEmpleadosCtrl'
  })

  .state('canjearPuntos', {
    url: '/page23',
    templateUrl: 'templates/canjearPuntos.html',
    controller: 'canjearPuntosCtrl'
  })

  .state('listaDeEmpleados', {
    url: '/page24',
    templateUrl: 'templates/listaDeEmpleados.html',
    controller: 'listaDeEmpleadosCtrl'
  })

  .state('compartirEvento', {
    url: '/page26',
    templateUrl: 'templates/compartirEvento.html',
    controller: 'compartirEventoCtrl'
  })

  .state('blogBienvenidosAGreenfund', {
    url: '/page27',
    templateUrl: 'templates/blogBienvenidosAGreenfund.html',
    controller: 'blogBienvenidosAGreenfundCtrl'
  })

  .state('historial', {
    url: '/page20',
    templateUrl: 'templates/historial.html',
    controller: 'historialCtrl'
  })

  .state('blog', {
    url: '/page22',
    templateUrl: 'templates/blog.html',
    controller: 'blogCtrl'
  })

  .state('donar', {
    url: '/page28',
    templateUrl: 'templates/donar.html',
    controller: 'donarCtrl'
  })

$urlRouterProvider.otherwise('/page9')

  

});